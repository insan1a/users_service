package main

import (
	"gitlab.com/insan1a/users_service/app"
	"log"
)

func main() {
	log.Fatal(app.Run())
}
