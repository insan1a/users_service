package app

import "gitlab.com/insan1a/orders_service/lib/httpserver"

func Run() error {
	httpServer := httpserver.New(httpserver.NewConfig(":8829", nil))
	return httpServer.Start()
}
