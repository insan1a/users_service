.PHONY: run
run: build
	./bin/users_service

.PHONY: build
build:
	go build -o ./bin/users_service ./cmd/main.go
